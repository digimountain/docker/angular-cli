FROM alpine:3.10.1

RUN chmod 777 /home
ENV HOME=/home
RUN apk update && apk add bash 

RUN apk add npm=10.16.3-r0 chromium udev ttf-freefont
RUN npm install -g @angular/cli@8.2.1
RUN chmod -R 777 /home
ENV CHROME_BIN /usr/bin/chromium-browser
